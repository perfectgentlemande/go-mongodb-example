package main

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/perfectgentlemande/go-mongodb-example/internal/database"
	"bitbucket.org/perfectgentlemande/go-mongodb-example/internal/service"
)

func main() {
	ctx := context.Background()

	db, err := database.NewDatabase(ctx, &database.Config{
		DBName:  "test",
		ConnStr: "mongodb://localhost:55000",
	})
	if err != nil {
		log.Fatal("cannot create db:", err)
	}
	defer db.Close(ctx)

	err = db.Ping(ctx)
	if err != nil {
		log.Fatal("cannot ping db:", err)
	}

	log.Println("successful")

	srvc := service.NewService(db)

	//sampleEmail := "test_user2@test.com"
	//err = srvc.CreateUser(ctx, &service.User{
	//	Username: "test_user2",
	//	Email:    &sampleEmail,
	//})
	//if err != nil {
	//	log.Fatal("cannot create user:", err)
	//}

	usrs, err := srvc.ListUsers(ctx)
	if err != nil {
		log.Fatal("cannot list users:", err)
	}

	for i := range usrs {
		fmt.Println(*usrs[i].Email)
	}

	fmt.Println(usrs)
}
