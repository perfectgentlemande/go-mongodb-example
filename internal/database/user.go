package database

import (
	"context"
	"fmt"

	"bitbucket.org/perfectgentlemande/go-mongodb-example/internal/service"
	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	ID       string  `bson:"_id"`
	Username string  `bson:"username"`
	Email    *string `bson:"email,omitempty"`
}

func (d *Database) CreateUser(ctx context.Context, user *service.User) error {
	_, err := d.userCollection.InsertOne(ctx, User{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
	})

	if err != nil {
		return fmt.Errorf("cannot insert user: %w", err)
	}

	return nil
}
func (d *Database) ListUsers(ctx context.Context) ([]service.User, error) {
	cur, err := d.userCollection.Find(ctx, bson.M{})
	if err != nil {
		return nil, fmt.Errorf("cannot find users: %w", err)
	}
	defer cur.Close(ctx)

	res := make([]service.User, 0)
	for cur.Next(ctx) {
		usr := User{}

		err = cur.Decode(&usr)
		if err != nil {
			return nil, fmt.Errorf("cannot decode user: %w", err)
		}

		res = append(res, service.User{
			ID:       usr.ID,
			Username: usr.Username,
			Email:    usr.Email,
		})
	}

	return res, nil
}
