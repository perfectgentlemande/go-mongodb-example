package service

import (
	"context"

	"github.com/google/uuid"
)

type User struct {
	ID       string
	Username string
	Email    *string
}

func (s *Service) ListUsers(ctx context.Context) ([]User, error) {
	return s.userStorage.ListUsers(ctx)
}

func (s *Service) CreateUser(ctx context.Context, user *User) error {
	user.ID = uuid.NewString()

	return s.userStorage.CreateUser(ctx, user)
}
